from utils import input_packages, packs_to_inventory

import os
import asyncio


INSTALL_PLAYBOOK_PATH = 'Installation.yaml'
UNINSTALL_PLAYBOOK_PATH = 'Uninstallation.yaml'

# Hack for debugging program with given docker and described Dockerfile
ANSIBLE_DEBUG_HOST_VARS = {
    'ansible_user': 'test',
    'ansible_password': 1,
    'ansible_become_user': 'root',
    'ansible_become_password': 1
}


def save_packages(groups, hosts_configured=True):
    os.makedirs("tmp", exist_ok=True)
    for i, g in enumerate(groups):
        with open(f'tmp/hosts{i}', 'w') as ansible_vars:
            if hosts_configured:
                ansible_vars.write(packs_to_inventory(g))
            else:
                ansible_vars.write(packs_to_inventory(g, ANSIBLE_DEBUG_HOST_VARS))


async def call_ansible(operation, hosts_configured, index, return_codes):
    process = await asyncio.create_subprocess_shell(
        f"ansible-playbook {'-i /etc/ansible/hosts' if hosts_configured else ''} -i tmp/hosts{index} {operation}",
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE
    )

    await process.communicate()
    # print(f'[exited with {process.returncode}]')
    # if stdout:
    #     print(f'[stdout]\n{stdout.decode()}')
    # if stderr:
    #     print(f'[stderr]\n{stderr.decode()}')

    return_codes.append(process.returncode)


def install_packages(groups):
    if len(groups) == 0:
        print("OK")
        return 0

    save_packages(groups)
    hosts_configured = os.path.exists("/etc/ansible/hosts")

    io_loop = asyncio.get_event_loop()
    tasks, return_codes = [], []

    for i, g in enumerate(groups):
        tasks.append(io_loop.create_task(call_ansible(INSTALL_PLAYBOOK_PATH, hosts_configured, i, return_codes)))

    io_loop.run_until_complete(asyncio.wait(tasks))

    if sum(return_codes) > 0:
        tasks, return_codes = [], []
        for i, g in enumerate(groups):
            tasks.append(io_loop.create_task(call_ansible(UNINSTALL_PLAYBOOK_PATH, hosts_configured, i, return_codes)))
        io_loop.run_until_complete(asyncio.wait(tasks))
        io_loop.close()
        if sum(return_codes) > 0:
            raise RuntimeError("Can not install packaged\nCan not roll back")
        print("FAIL")
        return 1
    io_loop.close()
    print("OK")
    return 0


if __name__ == "__main__":
    exit(install_packages(input_packages()))
