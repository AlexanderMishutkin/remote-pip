import json
import yaml


def solve_conflicts(groups):
    hosts = set(h for g in groups for h in g['hosts'])
    for host in hosts:
        if sum(host in g['hosts'] for g in groups) > 1:
            apt_packs = []
            pip_packs = []
            for g in groups:
                if host in g['hosts']:
                    apt_packs += g['apt_packs']
                    pip_packs += g['pip_packs']
                    g['hosts'].remove(host)
            groups.append({'hosts': [host], 'apt_packs': apt_packs, 'pip_packs': pip_packs})
    for g in groups[:]:
        if len(g['hosts']) == 0:
            groups.remove(g)
            

def merge(groups):
    # Order of packages matter
    def packs_to_str(apt, pip):
        return f"{','.join(apt)}\n{','.join(pip)}"
        
    packs = {}
    for g in groups:
        key = packs_to_str(g['apt_packs'], g['pip_packs'])
        packs[key] = packs.pop(key, []) + [g]

    res = []
    for pack in packs.values():
        res.append({'hosts': [], 'apt_packs': pack[0]['apt_packs'], 'pip_packs': pack[0]['pip_packs']})
        for g in pack:
            res[-1]['hosts'] += g['hosts']
    return res


def input_packages():
    s = input()
    result = []

    hosts = []
    apt_packs = []
    pip_packs = []

    while s != '' and s != 'q' and s != 'exit':
        args = s.split()
        if len(args) == 1 and len(apt_packs) + len(pip_packs) == 0:
            hosts.append(s)
        elif len(args) == 1 and len(apt_packs) + len(pip_packs) >= 1:
            result.append({'hosts': hosts, 'apt_packs': apt_packs, 'pip_packs': pip_packs})
            hosts = [s]
            apt_packs = []
            pip_packs = []
        elif len(args) >= 2 and len(hosts) == 0:
            raise ValueError(f"You can not install packages ({args}) until you mention at least one host")
        elif len(args) >= 2 and len(hosts) >= 1:
            if args[0] == 'apt':
                apt_packs += args[1:]
            elif args[0] == 'pip':
                pip_packs += args[1:]
            else:
                raise NotImplementedError(f"Current version of remote-pip support only apt's and pip's packages, "
                                          f"not {args[0]}'s")
        s = input()

    if len(hosts) >= 1 and len(apt_packs) + len(pip_packs) == 0:
        raise ValueError(f"You can not install something on hosts {args} until you mention at least one package")
    if len(apt_packs) + len(pip_packs) > 0:
        result.append({'hosts': hosts, 'apt_packs': apt_packs, 'pip_packs': pip_packs})

    solve_conflicts(result)
    return merge(result)


def packs_to_inventory(packs, debug_host_vars=None):
    return yaml.dump({
        'remote_pip_tmp': {
            'hosts': dict([(h, debug_host_vars) for h in packs['hosts']]),
            'vars': {
                'pip_packages_list': packs['pip_packs'],
                'apt_packages_list': packs['apt_packs'],
            }
        }
    })
