FROM ubuntu:latest
RUN apt update
EXPOSE 22
RUN apt --yes install openssh-server
RUN apt --yes install openssh-client
RUN apt --yes install sudo
RUN apt update && apt --yes install python3-pip
RUN systemctl enable ssh
RUN echo "PermitRootLogin yes" > /etc/ssh/sshd_config
RUN ( echo 1; echo 1) | passwd root
RUN useradd -rm -d /home/ubuntu -s /bin/bash -g root -G sudo -u 1000 test
RUN ( echo 1; echo 1) | passwd test
RUN service ssh start
CMD ["/usr/sbin/sshd","-D"]
